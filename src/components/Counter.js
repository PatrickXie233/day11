import { useState } from "react";
const Counter = ({ onChange, index, value }) => {
  const [count, setCount] = useState(0);
  const handleAdd = () => {
    const newCount = count + 1;
    setCount(newCount);
    onChange(index, newCount);
  };
  const handleReduce = () => {
    const newCount = count - 1;
    setCount(newCount - 1);
    onChange(index, newCount);
  };
  return (
    <div>
      <button onClick={handleAdd}>+</button>
      <span>{count}</span>
      <button onClick={handleReduce}>-</button>
    </div>
  );
};

export default Counter;
