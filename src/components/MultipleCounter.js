import { useState } from "react";
import CounterGroup from "./CounterGroup";
import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroupSum from "./CountGroupSum";

const MultipleCounter = () => {
  const [counterGroupList, setCountGroupList] = useState([]);
  const [sum, setSum] = useState(0);
  const handleChange = (size) => {
    const sizeNumber = Number(size);
    setCountGroupList(Array(sizeNumber).fill(0));
  };
  const handleSum = (index, value) => {
    let newCounterGroupList = [...counterGroupList];
    newCounterGroupList[index] = value;
    setCountGroupList(newCounterGroupList);
    console.log(newCounterGroupList);
    const sumNum = newCounterGroupList.reduce((pre, cur) => {
      return pre + cur;
    }, 0);

    setSum(sumNum);
    console.log(sum);
  };

  return (
    <div>
      <CounterSizeGenerator handleChange={handleChange}></CounterSizeGenerator>
      <CounterGroupSum sum={sum}></CounterGroupSum>
      <CounterGroup
        size={counterGroupList.length}
        handleChange={handleSum}
      ></CounterGroup>
    </div>
  );
};
export default MultipleCounter;
