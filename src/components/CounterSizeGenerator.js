import { useState } from "react";

const CounterSizeGenerator = (props) => {
  const [size, setSize] = useState(0);
  const onChange = (e) => {
    if (Number(e.target.value) <= 0) {
      props.handleChange(0);
      setSize(0);
      return;
    }
    setSize(e.target.value);
    props.handleChange(e.target.value);
  };
  return (
    <div>
      size: <input type="number" onChange={onChange} value={size}></input>
    </div>
  );
};
export default CounterSizeGenerator;
