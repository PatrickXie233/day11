import Counter from "./Counter";

const CounterGroup = (props) => {
  const counterSize = [...Array(props.size).keys()];
  return (
    <div>
      {counterSize.map((value, index) => {
        return (
          <Counter
            key={index}
            index={index}
            value={value}
            onChange={props.handleChange}
          ></Counter>
        );
      })}
    </div>
  );
};

export default CounterGroup;
